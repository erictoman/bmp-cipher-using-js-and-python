var cmd=require('node-cmd');
const testFolder = './ORIGINAL/';
const testFolder2 = './MODIFIED/';
var $ = require('jquery')
const fs = require("fs")
var image=""
var key0=""
var modo=""
var vector=""

function ModImage(image,dest,key,type,mode,enc,vector=""){
    console.log('py ./script.py '+image+" "+dest+" "+type+" "+mode+" "+key+" "+enc+" "+vector)
    cmd.get(
        'py ./script.py '+image+" "+dest+" "+type+" "+mode+" "+key+" "+enc+" "+vector,
        function(err, data, stderr){
            if(parseInt(data)==1){
                alert("Listo!")
                $("#img1").attr("src",image+"?a="+randInt());
                $("#img2").attr("src",dest+"?a="+randInt());
            }else{
                alert("Error!")
            }
        }
    )
}

$(() => {
        $("#ENC").click(function(){
            if(checkKeys()){
                fs.readdirSync(testFolder).forEach(file => {
                    $("#img1").attr("src",testFolder+file);
                    image=testFolder+file
                    dest=testFolder2+type+'-'+modo+"-"+file
                    ModImage(image,dest,key0,type,modo,1,vector)
                })
            }
        })
        $("#DEC").click(function(){
            if(checkKeys()){
                fs.readdirSync(testFolder).forEach(file => {
                    $("#img1").attr("src",testFolder+file);
                    image=testFolder+file
                    dest=testFolder2+type+'-'+modo+"-"+file
                    ModImage(image,dest,key0,type,modo,0,vector)
                })
            }
        })
})

function randInt(){
    return Math.random();
}

function checkKeys(){
    key0 = $("#key0").val()
    type = $("#TIPO").val()
    modo = $("#MODO").val()
    vector = $("#vector").val()
    if($("#TIPO").val()==="AES"){
        if(key0.length==16 && vector.length>=16){
            return true
        }else{
            alert("Key must have 16 characters")
            return false
        }  
    }
    if($("#TIPO").val()==="DES"){
        if(key0.length==8 && vector.length>=8){
            return true
        }else{
            alert("Key must have 8 characters")
            return false
        } 
    }
    return true
}
vector = $('#vector').val()