from PIL import Image
from Crypto.Cipher import AES
from Crypto.Cipher import DES
import sys
IV = "A"*16
format="bmp"
 
# AES requires that plaintexts be a multiple of 16, so we have to pad the data
def pad(data):
    return data + b"\x00"*(16-len(data)%16)
def pad(data):
    return data + b"\x00"*(8-len(data)%8)
 
# Maps the RGB 
def convert_to_RGB(data):
    #r, g, b = tuple(map(lambda d: [data[i] for i in range(0,len(data)) if i % 3 == d], [0, 1, 2]))
    #pixels = tuple(zip(r,g,b))
    return data
    
def process_image(origin,dest,tp,mode,key,enc):
    try:
        # Opens image and converts it to RGB format for PIL
        im = Image.open(origin)
        data = im.convert("RGB").tobytes() 
    
        # Since we will pad the data to satisfy AES's multiple-of-16 requirement, we will store the original data length and "unpad" it later.
        original = len(data) 
    
        # Encrypts using desired AES mode (we'll set it to ECB by default)
        if tp=="AES":
            if mode=="CBC":
                if enc:
                    new = convert_to_RGB(aes_cbc_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(aes_cbc_decrypt(key, pad(data))[:original])
            elif mode=="ECB":
                if enc:
                    new = convert_to_RGB(aes_ecb_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(aes_ecb_decrypt(key, pad(data))[:original])
            elif mode=="CFB":
                if enc:
                    new = convert_to_RGB(aes_cfb_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(aes_cfb_decrypt(key, pad(data))[:original])
            elif mode=="OFB":
                if enc:
                    new = convert_to_RGB(aes_ofb_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(aes_ofb_decrypt(key, pad(data))[:original])
        else:
            if mode=="CBC":
                if enc:
                    new = convert_to_RGB(des_cbc_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(des_cbc_decrypt(key, pad(data))[:original])
            elif mode=="ECB":
                if enc:
                    new = convert_to_RGB(des_ecb_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(des_ecb_decrypt(key, pad(data))[:original])
            elif mode=="CFB":
                if enc:
                    new = convert_to_RGB(des_cfb_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(des_cfb_decrypt(key, pad(data))[:original])
            elif mode=="OFB":
                if enc:
                    new = convert_to_RGB(des_ofb_encrypt(key, pad(data))[:original])
                else:
                    new = convert_to_RGB(des_ofb_decrypt(key, pad(data))[:original])
        # Create a new PIL Image object and save the old image data into the new image.
        im2 = Image.frombytes("RGB",im.size, new, "raw", "RGB").save(dest,format="bmp")
        print(1)
    except Exception as e:
        print(e)
        print(0)
#ENCRYPT AES
# CBC
def aes_cbc_encrypt(key, data, mode=AES.MODE_CBC):
    aes = AES.new(key.encode("utf-8"), mode, IV.encode("utf-8"))
    new_data = aes.encrypt(data)
    return new_data
# ECB
def aes_ecb_encrypt(key, data, mode=AES.MODE_ECB):
    aes = AES.new(key.encode("utf-8"), mode)
    new_data = aes.encrypt(data)
    return new_data
#CFB
def aes_cfb_encrypt(key, data, mode=AES.MODE_CFB):
    aes = AES.new(key.encode("utf-8"), mode, IV.encode("utf-8"))
    new_data = aes.encrypt(data)
    return new_data
#OFB
def aes_ofb_encrypt(key, data, mode=AES.MODE_OFB):
    aes = AES.new(key.encode("utf-8"), mode, IV.encode("utf-8"))
    new_data = aes.encrypt(data)
    return new_data

#ENCRYPT DES
def des_cbc_encrypt(key, data, mode=DES.MODE_CBC):
    des = DES.new(key.encode("utf-8"), mode,IV.encode("utf-8"))
    new_data = des.encrypt(data)
    return new_data

def des_ecb_encrypt(key, data, mode=DES.MODE_ECB):
    des = DES.new(key.encode("utf-8"), mode)
    new_data = des.encrypt(data)
    return new_data

def des_cfb_encrypt(key, data, mode=DES.MODE_CFB):
    des = DES.new(key.encode("utf-8"), mode,IV.encode("utf-8"))
    new_data = des.encrypt(data)
    return new_data

def des_ofb_encrypt(key, data, mode=DES.MODE_OFB):
    des = DES.new(key.encode("utf-8"), mode,IV.encode("utf-8"))
    new_data = des.encrypt(data)
    return new_data
#DECRIPT AES

# CBC
def aes_cbc_decrypt(key, data, mode=AES.MODE_CBC):
    aes = AES.new(key.encode("utf-8"), mode, IV.encode("utf-8"))
    new_data = aes.decrypt(data)
    return new_data
# ECB
def aes_ecb_decrypt(key, data, mode=AES.MODE_ECB):
    aes = AES.new(key.encode("utf-8"), mode)
    new_data = aes.decrypt(data)
    return new_data
#CFB
def aes_cfb_decrypt(key, data, mode=AES.MODE_CFB):
    aes = AES.new(key.encode("utf-8"), mode, IV.encode("utf-8"))
    new_data = aes.decrypt(data)
    return new_data
#OFB
def aes_ofb_decrypt(key, data, mode=AES.MODE_OFB):
    aes = AES.new(key.encode("utf-8"), mode, IV.encode("utf-8"))
    new_data = aes.decrypt(data)
    return new_data
#DECRIPT DES
def des_cbc_decrypt(key, data, mode=DES.MODE_CBC):
    des = DES.new(key.encode("utf-8"), mode)
    new_data = des.decrypt(data)
    return new_data

def des_ecb_decrypt(key, data, mode=DES.MODE_ECB):
    des = DES.new(key.encode("utf-8"), mode)
    new_data = des.decrypt(data)
    return new_data

def des_cfb_decrypt(key, data, mode=DES.MODE_CFB):
    des = DES.new(key.encode("utf-8"), mode)
    new_data = des.decrypt(data)
    return new_data

def des_ofb_decrypt(key, data, mode=DES.MODE_OFB):
    des = DES.new(key.encode("utf-8"), mode)
    new_data = des.decrypt(data)
    return new_data
    
if __name__ == "__main__":
    origin=(sys.argv[1])#Path de origen
    dest=(sys.argv[2])#Path de destino
    tp=(sys.argv[3])#Tipo AES DES
    mode=(sys.argv[4])#MODO CBC ECB
    key=(sys.argv[5])#KEY AES 16 BYTE DES 8 BYTE
    enc=int((sys.argv[6]))# ENC 1 DEC 0
    if len(sys.argv)==7:
        IV='A'*8
        process_image(origin,dest,tp,mode,key,enc)
    else:
        IV=sys.argv[7] #IV
        process_image(origin,dest,tp,mode,key,enc)